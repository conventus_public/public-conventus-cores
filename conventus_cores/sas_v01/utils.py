import logging
import os

import sentry_sdk

sentry_sdk.init(os.getenv("SENTRY_DSN"))
logger = logging.getLogger(__name__)


unexpected_type_exception = Exception("unexpected_type_exception")


def cast_float_metrics(exception_enable=True, **kwargs):
    float_metrics = {}
    for metric_name, metric_value in kwargs.items():
        exc = None
        if not isinstance(metric_value, float):
            exc = Exception(f"Unexpected type exception, metric {metric_name} unexpected type = {type(metric_value)}")
            metric_value = 0
        elif metric_value > 1.0:
            exc = Exception(f"Out of max value, metric {metric_name} high value = {metric_value}")
            metric_value = 1.0
        elif metric_value < 0.0:
            exc = Exception(f"Out of min value, metric {metric_name} low value = {metric_value}")
            metric_value = 0
        if exc:
            sentry_sdk.capture_exception(exc)
            logger.error(exc)
            if exception_enable:
                raise exc
        float_metrics[metric_name] = metric_value
    return float_metrics


def cast_int_metrics(exception_enable=True, **kwargs):
    float_metrics = {}
    for metric_name, metric_value in kwargs.items():
        exc = None
        try:
            metric_value = int(metric_value)
        except Exception:
            exc = Exception(f"Unexpected type exception, metric {metric_name} unexpected type = {type(metric_value)}")
            metric_value = None

        if metric_value not in [0, 1, 2, 3, 4]:
            exc = Exception(f"Out of max value, metric {metric_name} high value = {metric_value}")
            metric_value = -1
        if exc:
            sentry_sdk.capture_exception(exc)
            logger.error(exc)
            if exception_enable:
                raise exc
        float_metrics[metric_name] = metric_value
    return float_metrics


def cast_metrics(
    involvement=None,
    awareness=None,
    clarity=None,
    mental_flexibility=None,
    anxiety=None,
    psy_type=None,
    exception_enable=True,
    **kwargs,
):
    float_metrics = cast_float_metrics(
        involvement=involvement,
        awareness=awareness,
        clarity=clarity,
        mental_flexibility=mental_flexibility,
        anxiety=anxiety,
        exception_enable=exception_enable,
    )
    int_metrics = cast_int_metrics(psy_type=psy_type, exception_enable=exception_enable)
    kwargs.update(float_metrics)
    kwargs.update(int_metrics)
    return kwargs
