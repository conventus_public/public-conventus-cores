import pathlib
import json
import random

work_dir = pathlib.Path(__file__).parent


def get_random_test_function(core_name, model_name):
    test_dir = work_dir / core_name / "public" / model_name / "tests"
    func_outputs = [json.load(file.open()) for file in test_dir.glob("./*_out.json")]

    def emulator(*args, **kwargs):
        seed = hash(f"{str(args)}:{str(kwargs)}")
        random.seed(seed)
        return random.choice(func_outputs)

    return emulator
