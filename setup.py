import os
import pathlib

import setuptools

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))


def read_requirements():
    """parses requirements from requirements.txt"""
    reqs_path = os.path.join(__location__, "requirements.txt")
    with open(reqs_path, encoding="utf8") as f:
        reqs = [line.strip() for line in f if not line.strip().startswith("#")]

    names = []
    links = []
    for req in reqs:
        if "://" in req:
            links.append(req)
        else:
            names.append(req)
    return {"install_requires": names, "dependency_links": links}


conventus_cores_dir = pathlib.Path("conventus_cores")

setuptools.setup(
    name=os.path.dirname(__file__).split("/")[-1],
    version="0.0.1",
    include_package_data=True,
    description="",
    long_description="",
    keywords=[],
    packages=setuptools.find_packages(),
    python_requires=">=3.7",
    url="",
    data_files=[
        (
            "conventus_cores",
            [
                str(file)
                for file in list(conventus_cores_dir.glob("./**/*.json")) + list(conventus_cores_dir.glob("./**/*.csv"))
            ],
        )
    ],
    **read_requirements()
)
