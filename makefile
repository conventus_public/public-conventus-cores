SHELL:=/bin/bash

# .PHONY: ...

# run by default up_env
all: public_release
# install shortcuts
public_release:
	rm -rf public_conventus_cores/*
	mkdir -p public_conventus_cores
	cp -rf conventus_cores public_conventus_cores/conventus_cores
	find . -maxdepth 1 -type f -exec cp -rf {} public_conventus_cores \;



	

