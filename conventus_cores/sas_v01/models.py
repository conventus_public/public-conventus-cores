import pathlib


import conventus_cores.utils as cores_utils
import conventus_cores.sas_v01.utils as core_utils

core_name = pathlib.Path(__file__).parent.name

core_models = {
    "keep_employee": cores_utils.get_random_test_function(core_name, "keep_employee"),

}


def keep_employee(
    involvement=None,
    awareness=None,
    clarity=None,
    mental_flexibility=None,
    anxiety=None,
    psy_type=None,
    **kwargs,
):
    metrics = core_utils.cast_metrics(
        involvement=involvement,
        awareness=awareness,
        clarity=clarity,
        mental_flexibility=mental_flexibility,
        anxiety=anxiety,
        psy_type=psy_type,

        **kwargs,
    )
    involvement = metrics.get("involvement", 0)
    awareness = metrics.get("awareness", 0)
    clarity = metrics.get("clarity", 0)
    mental_flexibility = metrics.get("mental_flexibility", 0)
    anxiety = metrics.get("anxiety", 0)
    psy_type = metrics.get("psy_type", 0)

    return core_models["keep_employee"](
        involvement=involvement,
        awareness=awareness,
        clarity=clarity,
        mental_flexibility=mental_flexibility,
        anxiety=anxiety,
        psy_type=psy_type,
        **kwargs,
    )
